import asyncio
import logging
from collections import OrderedDict

import proxy_dispatcher.checkers as checkers

logger = logging.getLogger(__name__)


class Proxy(object):

  # Mapping between service names and their checkers
  services_mapping = OrderedDict({
    "google_search": checkers.check_google_search
  })

  def __init__(self, remote_addr):
    self.remote_addr = remote_addr
    self.in_use = False
    self.supported_services = {k: False for k in self.services_mapping.keys()}

  def __eq__(self, other):
    return self.remote_addr == other.remote_addr

  def __ne__(self, other):
    return not self.remote_addr == other.remote_addr

  def __str__(self):
    return str(self.remote_addr)

  def __repr__(self):
    return self.__str__()

  async def check_services(self):
    """
    Performs checks for each service from services_mapping and sets appropriate flag in self.supported_services
    """
    services = list(self.services_mapping.keys())
    checkers = self.services_mapping.values()
    tasks = [asyncio.ensure_future(checker(self.remote_addr)) for checker in checkers]
    results = await asyncio.gather(*tasks)

    for i, result in enumerate(results):
      self.supported_services[services[i]] = result
