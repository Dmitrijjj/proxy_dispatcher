import asyncio
import random
import logging

from aiohttp import ClientSession, TCPConnector
from aiohttp.errors import ProxyConnectionError, HttpProxyError, ServerDisconnectedError, ClientOSError, ClientResponseError

logger = logging.getLogger(__name__)


async def check_google_search(proxy):
  """
    Checks the validity of the provided proxy
    :return: True if proxy os ok,else False
  """
  headers = {
    'Upgrade-Insecure-Requests': '1',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, sdch, br',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    'Host': 'www.google.com',
    # OperaMini
    'User-Agent': 'Opera/9.80 (Android; Opera Mini/8.0.1807/36.1609; U; en) Presto/2.12.423 Version/12.16'
  }
  queries = ["apple", "iphone", "samsung", "microsoft", "google"]
  conn = TCPConnector(verify_ssl=False)
  async with ClientSession(connector=conn) as sess:
    try:
      async with sess.get("https://www.google.com/search?q={}&oq=".format(random.choice(queries)),
                          proxy="http://" + proxy, headers=headers, timeout=5) as r:
        return True if r.status == 200 else False
    except (asyncio.TimeoutError, ProxyConnectionError, ConnectionRefusedError, ServerDisconnectedError, ClientOSError,
            ClientResponseError):
      return False
    except HttpProxyError as e:
      # Bad Gateway
      if e.code != 502:
        logger.exception(e)
      return False
    except Exception as e:
      logger.exception(e)
      return False
