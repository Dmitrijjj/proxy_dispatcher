import asyncio
import logging
from collections import deque

from aiohttp import ClientSession, TCPConnector
from aiohttp.errors import ProxyConnectionError, HttpProxyError, ServerDisconnectedError, ClientResponseError, \
  ClientOSError

from proxy_dispatcher.proxy import Proxy
from proxy_dispatcher.proxy_server import ProxyServer

logger = logging.getLogger(__name__)


class ProxyServerManager(object):
  _instance = None

  def __new__(self, *args, **kwargs):
    if self._instance is None:
      self._instance = super(ProxyServerManager, self).__new__(self)
      self._instance._initialized = False
    return self._instance

  def __init__(self, my_ip=None, loop=None, renewal_url=None):
    if self._initialized:
      return
    self.available_proxies = deque(maxlen=5000)
    self.hot_proxies_len = 60
    self.hot_proxies = deque(maxlen=self.hot_proxies_len)
    self.hot_chunk_size = 10
    self.new_proxies_queue = asyncio.Queue()
    self.check_services_queue = asyncio.Queue()
    self.running_servers = {}
    self.loop = loop
    self.my_ip = my_ip

    # Updates proxies aynchorous
    self.renewal_url = renewal_url
    if renewal_url:
      asyncio.ensure_future(self.renewal_worker())

    asyncio.ensure_future(self.check_new_proxies_worker())
    asyncio.ensure_future(self.check_services_worker())
    asyncio.ensure_future(self.hot_proxies_watcher())
    asyncio.ensure_future(self.hot_proxies_checker())
    self._initialized = True

  async def renewal_worker(self):
    """
    Each time period checks for new proxies
    """
    while True:
      async with ClientSession(connector=TCPConnector(verify_ssl=False)) as sess:
        async with sess.get(self.renewal_url, timeout=10) as r:
          resp = await r.read()
      new_proxies = resp.decode("utf-8").split("\n")
      # Split proxies into chunks and put to the check queue
      chunks = [new_proxies[i:i + 50] for i in range(0, len(new_proxies), 50)]
      # Flush queues
      self.new_proxies_queue._queue.clear()
      self.check_services_queue._queue.clear()
      for ch in chunks:
        await self.new_proxies_queue.put(ch)
      await asyncio.sleep(7 * 60)

  async def check_new_proxies_worker(self):
    """
    Performs availability check for new proxies. Then adds them to the available proxies pool and passes them
    to the check service queue.
    """
    while True:
      proxies = await self.new_proxies_queue.get()
      # Async checking availability of proxy
      tasks = [asyncio.ensure_future(self.check_availability(p)) for p in proxies]
      results = await asyncio.gather(*tasks)

      available_proxies = [Proxy(proxies[i]) for i, res in enumerate(results) if res is True]
      for ap in available_proxies:
        if ap in self.available_proxies:
          continue
        self.available_proxies.append(ap)
        # Check availability for certain services
        await self.check_services_queue.put(available_proxies)

  @staticmethod
  async def check_availability(proxy):
    """
    Try to connect to site via proxy
    :return: True if proxy availabile, False otherwise
    """
    conn = TCPConnector(verify_ssl=False)
    async with ClientSession(connector=conn) as sess:
      try:
        async with sess.get("http://www.soccer.ru/", proxy="http://" + str(proxy), timeout=3) as r:
          if r.status == 200:
            return True
      except (ProxyConnectionError, asyncio.TimeoutError, ConnectionRefusedError, ServerDisconnectedError,
              ClientOSError, ClientResponseError) as e:
        return False
      except HttpProxyError as e:
        # Bad Gateway
        if e.code != 502:
          logger.exception(e)
        return False
      except Exception as e:
        logger.exception(e)
        return False
    return False

  async def check_services_worker(self):
    """
    Waits for the batch with available proxies from queue and calls proxy methods which
    perform checks for specific services (e.g. google_search) for each proxy.
    """
    while True:
      proxies_chunk = await self.check_services_queue.get()
      tasks = [asyncio.ensure_future(proxy.check_services()) for proxy in proxies_chunk]
      await asyncio.gather(*tasks)

  async def hot_proxies_watcher(self):
    """
    Ensures that pool of hot proxies is always full.
    """
    while True:
      diff = self.hot_proxies_len - len(self.hot_proxies) - self.hot_chunk_size
      while diff > 0:
        try:
          self.hot_proxies.appendleft(self.available_proxies.pop())
          diff -= 1
        except IndexError:
          await asyncio.sleep(1)
      await asyncio.sleep(1)

  async def hot_proxies_checker(self):
    """
    Constantly performs availability checks for hot proxies
    """
    while True:
      hot_proxies_chunk = []
      while len(hot_proxies_chunk) < self.hot_chunk_size:
        try:
          hot_proxies_chunk.append(self.hot_proxies.popleft())
        except IndexError:
          await asyncio.sleep(1)
      tasks = [asyncio.ensure_future(self.check_availability(p)) for p in hot_proxies_chunk]
      results = await asyncio.gather(*tasks)
      available_proxies = [hot_proxies_chunk[i] for i, res in enumerate(results) if res is True]
      for ap in available_proxies:
        self.hot_proxies.append(ap)

  async def start_new_server(self, service=None):
    """
    Start new server according to currnt configure
    :return Tuple with server id and address
    """
    try:
      proxy = await self.get_proxy(service)
    except IndexError:
      raise NoAvailableProxyError("All proxies are in use. Try again later.")

    proxy.in_use = True
    new_server = ProxyServer(self.my_ip, self.loop, proxy)

    await new_server.start()

    self.running_servers[new_server.id] = new_server
    new_server_addr = "%s:%s" % (self.my_ip, new_server.local_port)
    return new_server.id, new_server_addr

  async def stop_server(self, server_id):
    """
    Stop server
    """
    running_server = self.running_servers.pop(server_id, None)
    if running_server is not None:
      proxy = running_server.proxy
      running_server.server.close()
      proxy.in_use = False

  @staticmethod
  def get_service_names():
    return list(Proxy.services_mapping.keys())

  async def get_proxy(self, service=None):
    """
    Return proxy from hot queue, otherwise from common queue
    :return hot proxy if service is not specified
    """
    if service is None and len(self.hot_proxies) > 0:
      return self.hot_proxies.pop()

    if service in self.get_service_names():
      for proxy in self.available_proxies:
        if proxy.supported_services[service] is True and not proxy.in_use:
          return proxy
    else:
      raise InvalidServiceError("Service is not supported: {}".format(service))


class NoAvailableProxyError(Exception):
  pass


class InvalidServiceError(Exception):
  pass
