import argparse
import asyncio
import logging

from aiohttp import web

from proxy_dispatcher.handlers import status_handler, get_proxy_handler, redirect_handler, ProxyView
from proxy_dispatcher.proxy_manager import ProxyServerManager


def init(loop, my_ip, renewal_url=None, log_level="INFO",
         log_format="%(asctime)s %(levelname)s %(name)s %(message)s"):
  """
  Init Proxy Manager
  """
  ProxyServerManager(my_ip, loop, renewal_url)
  level = logging.getLevelName(log_level)
  logging.basicConfig(level=level, format=log_format)


def make_app():
  """
  Main router
  :return:  web.Application
  """
  app = web.Application()
  app.router.add_get("/", redirect_handler)
  app.router.add_get("/status", status_handler)
  app.router.add_get("/get_proxy", get_proxy_handler)
  app.router.add_route("GET", "/proxies", ProxyView)
  app.router.add_route("*", "/proxies/{proxy_id:[a-z0-9-]+}", ProxyView)
  return app


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Reverse proxy service for accessing ip-bound proxies")
  parser.add_argument("--bind-ip", dest="bind_ip", required=True, help="bind ip")
  parser.add_argument("--bind-port", dest="bind_port", required=True, type=int, help="bind port")
  parser.add_argument("--renewal-url", dest="renewal_url", required=True, help="url with new proxies")
  parser.add_argument("--log-level", dest="log_level", default="INFO", help="log level")
  parser.add_argument("--log-format", dest="log_format", default="%(asctime)s %(levelname)s %(name)s %(message)s",
                      help="log format string")
  args = parser.parse_args()
  loop = asyncio.get_event_loop()
  my_ip = args.bind_ip
  init(loop, my_ip, args.renewal_url, args.log_level, args.log_format)
  app = make_app()
  web.run_app(app, host=my_ip, port=args.bind_port)
