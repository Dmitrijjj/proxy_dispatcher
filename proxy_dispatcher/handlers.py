import logging
from aiohttp import web

from proxy_dispatcher.proxy_manager import ProxyServerManager, NoAvailableProxyError, InvalidServiceError


logger = logging.getLogger(__name__)


async def status_handler(request):
  """
  Returns info about available proxies and active sessions in JSON.
  :return: JSON with summary info
  """
  try:
    services_info = {x: len([y for y in ProxyServerManager().available_proxies if
                     y.in_use is False and y.supported_services[x] is True]) for x in
                     ProxyServerManager().get_service_names()}
    data = {
      "proxies_available": len(ProxyServerManager().available_proxies),
      "hot_proxies_available": len(ProxyServerManager().hot_proxies),
      "services_proxies_available": services_info,
      "proxies_in_use": len(ProxyServerManager().running_servers)
    }
  except Exception as e:
    logger.exception(e)
    data = {"error": "{}: {}".format(e.__class__.__name__, str(e))}
  return web.json_response(data)


async def get_proxy_handler(request):
  """
  Start new proxy server
  """
  try:
    service = request.GET.get("service")
    server_id, server_addr = await ProxyServerManager().start_new_server(service=service)
    data = {"proxy_id": server_id, "proxy_addr": server_addr}
  except (NoAvailableProxyError, InvalidServiceError) as e:
    data = {"error": str(e)}
  except Exception as e:
    logger.exception(e)
    data = {"error": "{}: {}".format(e.__class__.__name__, str(e))}
  return web.json_response(data)


async def redirect_handler(request):
  """
  Redirects the request to a specialized
  """
  return web.HTTPFound("/status")


class ProxyView(web.View):

  async def get(self):
    """
    List all active proxy servers or get detailed info about specific running server
    :return: JSON with summary info
    """
    try:
      proxy_server_id = self.request.match_info.get("proxy_id")
      if proxy_server_id is None:
        # Return list of active proxy servers
        summary = []
        for server_id, server_instance in ProxyServerManager().running_servers.items():
          summary.append(
            {"id": server_id, "addr": "%s:%s" % (ProxyServerManager().my_ip, server_instance.local_port),
             "remote_addr": "%s:%s" % (server_instance.remote_address, server_instance.remote_port)})
        data = {"proxies": summary}
      else:
        # Return info about specific proxy server
        data = {}
    except Exception as e:
      logger.exception(e)
      data = {"error": "{}: {}".format(e.__class__.__name__, str(e))}
    return web.json_response(data)

  async def delete(self):
    """
    Stop server and return proxy to the pull of available proxies
    :return: JSON with summary info
    """
    try:
      data = {}
      proxy_server_id = self.request.match_info.get("proxy_id")
      if proxy_server_id:
        await ProxyServerManager().stop_server(proxy_server_id)
    except Exception as e:
      logger.exception(e)
      data = {"error": "{}: {}".format(e.__class__.__name__, str(e))}
    return web.json_response(data)
