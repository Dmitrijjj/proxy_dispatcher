import asyncio
import logging
import socket
import uuid


class ProxyServer(object):

  # Connection timeout in seconds
  timeout = 5
  # Size of the reader buffer in bytes
  reader_buf_size = 65536

  def __init__(self, my_ip, loop, proxy):
    self.id = str(uuid.uuid4())
    self.logger = logging.getLogger("%s %s" % (self.__class__.__name__, self.id))
    self.loop = loop
    self.my_ip = my_ip
    self.proxy = proxy
    self.remote_address, self.remote_port = proxy.remote_addr.split(":")

  async def start(self):
    """
    Start proxy server
    """
    self.server = await asyncio.start_server(self.handle_client, host=self.my_ip, port=0, family=socket.AF_INET)
    sock = self.server.sockets[0].getsockname()
    self.local_port = sock[1]
    self.logger.info('Starting proxy server on {}'.format(sock))

  def handle_client(self, client_reader, client_writer):
    asyncio.ensure_future(self.accept_client(client_reader=client_reader, client_writer=client_writer,
                                             remote_address=self.remote_address, remote_port=self.remote_port))

  async def proxy_data(self, reader, writer, connection_string):
    try:
      while True:
        data = await reader.read(self.reader_buf_size)
        if not data:
          break
        writer.write(data)
        await writer.drain()
    except Exception as e:
      self.logger.info('proxy_data_task exception {}'.format(e))
    finally:
      writer.close()
      self.logger.info('close connection {}'.format(connection_string))

  async def accept_client(self, client_reader, client_writer, remote_address, remote_port):
    client_string = self.get_conn_string(client_writer)
    self.logger.info('accept connection {}'.format(client_string))
    try:
      remote_reader, remote_writer = await asyncio.wait_for(asyncio.open_connection(
        host=remote_address, port=remote_port), timeout=self.timeout)
    except asyncio.TimeoutError:
      self.logger.info('close connection {} due to connection timeout'.format(client_string))
      client_writer.close()
    except Exception as e:
      self.logger.info('error connecting to remote server: {}'.format(e))
      self.logger.info('close connection {}'.format(client_string))
      client_writer.close()
    else:
      remote_string = self.get_conn_string(remote_writer, remote=True)
      self.logger.info('connected to remote {}'.format(remote_string))
      asyncio.ensure_future(self.proxy_data(client_reader, remote_writer, remote_string))
      asyncio.ensure_future(self.proxy_data(remote_reader, client_writer, client_string))

  @staticmethod
  def get_conn_string(writer, remote=False):
    """
    Connection status
    :return: String, including connection status
    """
    peer_host, peer_port = writer.get_extra_info('peername')
    sock_host, sock_port = writer.get_extra_info('sockname')
    return '%s:%s -> %s:%s' % (peer_host, peer_port, sock_host, sock_port) if remote is False \
      else '%s:%s -> %s:%s' % (sock_host, sock_port, peer_host, peer_port)
