import asyncio
import random

from aiohttp import ClientSession, TCPConnector


async def get_proxy_and_fetch_urls(proxy_service_addr, fetch_num):
    """
    The basic version of test.
    Validates a returns a proxy, and the validity of proxies
    """
    sites = ["https://www.wikipedia.org/", "http://tass.ru/", "https://vk.com/", "https://lenta.ru/", "https://ok.ru/"]
    connector = TCPConnector(verify_ssl=False)
    async with ClientSession(connector=connector) as session:
        async with session.get("http://" + proxy_service_addr + "/get_proxy") as r:
            resp = await r.json()
        try:
            proxy_addr = resp["proxy_addr"]
            proxy_id = resp["proxy_id"]
        except KeyError as e:
            print("KeyError: {}. Response: {}".format(str(e), resp))
        try:
            # Make requests
            for _ in range(fetch_num):
                try:
                    site = random.choice(sites)
                    async with session.get(site, proxy="http://" + proxy_addr, timeout=10) as r:
                        if r.status != 200:
                            print("Error response code: {} from {} with proxy {}".format(r.status, site, proxy_addr))
                            continue
                        print("Proxy {} site {} OK".format(proxy_addr, site))
                except Exception as e:
                    print("Error while making request to {} with proxy {}: {}".format(site, proxy_addr, str(e)))
        except Exception as e:
            print(str(e))
        finally:
            # Return proxy
            async with session.delete("http://" + proxy_service_addr + "/proxies/" + proxy_id) as r:
                if r.status != 200:
                    resp = await r.read()
                    print("Error while deleting proxy {}: {}".format(proxy_id, resp))


if __name__ == "__main__":
    """
        The basic version of test.
        Start parallel threads, each try to get valid proxy and request popular web page
    """

    tasks_count = 50
    fetch_urls_per_task = 20

    tasks = [asyncio.ensure_future(get_proxy_and_fetch_urls("localhost:7979", fetch_urls_per_task))
             for _ in range(tasks_count)]
    asyncio.get_event_loop().run_until_complete(asyncio.wait(tasks))
